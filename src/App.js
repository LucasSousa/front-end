import React from 'react';
import logo from './logo.svg';
import './App.css';

const apiUrl = process.env.REACT_APP_API_URL || 'https://google.com';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Olá RS/XP!
        </p>
        <p>
          API URL:
        </p>
        <a
          className="App-link"
          href={apiUrl}
          target="_blank"
          rel="noopener noreferrer"
        >
          {apiUrl}
        </a>
      </header>
    </div>
  );
}

export default App;
